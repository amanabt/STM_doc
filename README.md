# DOCUMENTATION for STM software version sim68a

>Programs Required  
>You should be having <a href="http://dia-installer.de/" target="_blank">dia</a> installed in your linux machine.

#####Converting *.dia* files in *.pdf* format  
>In terminal type  
`` dia --nosplash filename.dia --export filename.pdf``.  

######You can output this in other formats by just changing the output file extension.

>Example:  
`` dia --nosplash designdoc.dia --export --designdoc.pdf ``